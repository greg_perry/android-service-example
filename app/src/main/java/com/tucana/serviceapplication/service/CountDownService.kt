package com.tucana.serviceapplication.service

import android.app.*
import android.app.PendingIntent.*
import android.content.Intent
import android.os.*
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService
import com.tucana.serviceapplication.R
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import java.util.concurrent.TimeUnit

private const val NOTIFICATION_CHANNEL_ID = "Countdown Service"
private const val STATUS_BAR_NOTIFICATION_ID = 37
private const val COUNTER_DURATION_SECS = 30L
private const val COUNTER_INTERVAL_SECS = 1L

class CountDownService : Service() {
    private val duration = TimeUnit.SECONDS.toMillis(COUNTER_DURATION_SECS)
    private val interval = TimeUnit.SECONDS.toMillis(COUNTER_INTERVAL_SECS)

    private val serviceBinder = ServiceBinder()

    private val _currentCount = MutableStateFlow(0L)
    val currentCount = _currentCount.asStateFlow()

    private val _isCountdownFinished = MutableStateFlow(true)
    val isCountdownFinished = _isCountdownFinished.asStateFlow()

    private val countdownTimer = object : CountDownTimer(duration, interval) {
        override fun onTick(millisUntilFinished: Long) {
            _currentCount.value = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
        }

        override fun onFinish() {
            _isCountdownFinished.value = true
        }
    }

    fun stopService() {
        stopForeground(true)
        stopSelf()
    }

    fun startCountdown() {
        countdownTimer.start()
        _isCountdownFinished.value = false
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT >= 26) {
            // Promote service to foreground (API 26+) required to prevent being killed by android
            startForeground(STATUS_BAR_NOTIFICATION_ID, buildNotification())
        }
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder {
        return serviceBinder
    }

    // Persistent notification to be displayed in the status bar for foreground services
    private fun buildNotification(): Notification =
        NotificationCompat.Builder(this, createNotificationChannel())
            .setSmallIcon(R.drawable.ic_countdown_notification)
            .setContentTitle(getString(R.string.notification_title))
            .setContentText(getString(R.string.notification_content))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()

    // Notification channel required for API 26+
    private fun createNotificationChannel(): String {
        if (Build.VERSION.SDK_INT >= 26) {
            val name = getString(R.string.notification_channel_name)
            val description = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance)
            channel.description = description
            getSystemService<NotificationManager>()?.createNotificationChannel(channel)
        }
        return NOTIFICATION_CHANNEL_ID
    }

    inner class ServiceBinder : Binder() {
        fun getService(): CountDownService {
            return this@CountDownService
        }
    }
}
