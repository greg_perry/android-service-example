package com.tucana.serviceapplication.domain

import com.tucana.serviceapplication.service.CountDownService

class StartCountdownUseCase {
    fun startCountdown(serviceBinder: CountDownService.ServiceBinder) {
        serviceBinder.getService().startCountdown()
    }
}
