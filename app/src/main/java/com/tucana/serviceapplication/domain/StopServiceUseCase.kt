package com.tucana.serviceapplication.domain

import com.tucana.serviceapplication.service.CountDownService

class StopServiceUseCase {
    fun stopService(serviceBinder: CountDownService.ServiceBinder) {
        serviceBinder.getService().stopService()
    }
}
