package com.tucana.serviceapplication.domain

import com.tucana.serviceapplication.service.CountDownService
import kotlinx.coroutines.flow.StateFlow

class CurrentCountUseCase {
    fun getCurrentCount(serviceBinder: CountDownService.ServiceBinder): StateFlow<Long> =
        serviceBinder.getService().currentCount
}
