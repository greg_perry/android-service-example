package com.tucana.serviceapplication.domain

import com.tucana.serviceapplication.service.CountDownService
import kotlinx.coroutines.flow.StateFlow

class CurrentStatusUseCase {
    fun getCurrentStatus(serviceBinder: CountDownService.ServiceBinder): StateFlow<Boolean> =
        serviceBinder.getService().isCountdownFinished
}
