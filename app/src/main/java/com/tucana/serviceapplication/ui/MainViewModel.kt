package com.tucana.serviceapplication.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tucana.serviceapplication.R
import com.tucana.serviceapplication.domain.*
import com.tucana.serviceapplication.service.CountDownService
import kotlinx.coroutines.flow.*

class MainViewModel(
    private val currentCountUseCase: CurrentCountUseCase,
    private val currentStatusUseCase: CurrentStatusUseCase,
    private val startCountdownUseCase: StartCountdownUseCase,
    private val stopServiceUseCase: StopServiceUseCase
) : ViewModel() {

    private val _buttonsEnabled = MutableStateFlow(true)
    val buttonsEnabled = _buttonsEnabled.asStateFlow()

    private fun status(hasFinished: Boolean): Int {
        _buttonsEnabled.value = hasFinished
        return if (hasFinished) R.string.finished else R.string.running
    }

    fun startCountdown(serviceBinder: CountDownService.ServiceBinder) {
        startCountdownUseCase.startCountdown(serviceBinder)
    }

    fun stopService(serviceBinder: CountDownService.ServiceBinder) {
        stopServiceUseCase.stopService(serviceBinder)
        _buttonsEnabled.value = false
    }

    suspend fun countFlow(serviceBinder: CountDownService.ServiceBinder): Flow<String> =
        currentCountUseCase.getCurrentCount(serviceBinder).map { it.toString() }.stateIn(viewModelScope)

    suspend fun statusFlow(serviceBinder: CountDownService.ServiceBinder): Flow<Int> =
        currentStatusUseCase.getCurrentStatus(serviceBinder).map { status(it) }.stateIn(viewModelScope)
}
