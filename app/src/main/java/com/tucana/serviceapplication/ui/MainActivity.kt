package com.tucana.serviceapplication.ui

import android.content.*
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.tucana.serviceapplication.databinding.ActivityMainBinding
import com.tucana.serviceapplication.service.CountDownService
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding
    private lateinit var serviceBinder: CountDownService.ServiceBinder
    private val viewModel by viewModel<MainViewModel>()

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            serviceBinder = binder as CountDownService.ServiceBinder
            observeCountdownEvents()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            // Unused
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        viewBinding.startCountdownButton.setOnClickListener {
            viewModel.startCountdown(serviceBinder)
        }

        viewBinding.stopServiceButton.setOnClickListener {
            viewModel.stopService(serviceBinder)
        }
    }

    override fun onStart() {
        super.onStart()
        startService()
    }

    override fun onStop() {
        super.onStop()
        unbindService(serviceConnection)
    }

    private fun startService() {
        Intent(this, CountDownService::class.java).also { countdownServiceIntent ->
            if (Build.VERSION.SDK_INT >= 26) {
                startForegroundService(countdownServiceIntent)
            }
            else {
                startService(countdownServiceIntent)
            }
            bindService(countdownServiceIntent, serviceConnection, BIND_AUTO_CREATE)
        }
    }

    private fun observeCountdownEvents() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                awaitAll(
                    async {
                        viewModel.countFlow(serviceBinder).collect { currentCount ->
                            viewBinding.countdownText.text = currentCount
                        }
                    },
                    async {
                        viewModel.statusFlow(serviceBinder).collect { runStatus ->
                            viewBinding.countdownStatusText.setText(runStatus)
                        }
                    },
                    async {
                        viewModel.buttonsEnabled.collect { isEnabled ->
                            viewBinding.apply {
                                stopServiceButton.isEnabled = isEnabled
                                startCountdownButton.isEnabled = isEnabled
                            }
                        }
                    })
            }
        }
    }
}
