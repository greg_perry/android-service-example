package com.tucana.serviceapplication

import android.app.Application
import com.tucana.serviceapplication.di.AppModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    // Initialise Koin DI framework and build dependency graph
    private fun initKoin() {
        startKoin {
            androidContext(this@MainApplication)
            modules(AppModule.presentationLayer, AppModule.domainLayer)
            androidLogger(Level.NONE)
        }
    }
}
