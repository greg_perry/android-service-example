package com.tucana.serviceapplication.di

import com.tucana.serviceapplication.domain.*
import com.tucana.serviceapplication.ui.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Constructs dependencies and builds the dependency graph for injected components
 */
object AppModule {
    val presentationLayer = module {
        viewModel { MainViewModel(get(), get(), get(), get()) }
    }

    val domainLayer = module {
        single { CurrentCountUseCase() }
        single { CurrentStatusUseCase() }
        single { StartCountdownUseCase() }
        single { StopServiceUseCase() }
    }
}
